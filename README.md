Install:

Clone the project.

Create an .env file in your root directory.

Copy the .env_example content.

Edit the database variables.

Run:

composer install

php artisan migrate

php artisan db:seed

Run the following url:

http://your_domain.test/test/{id}