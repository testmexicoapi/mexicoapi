<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $content = fopen(Storage::path("CPdescarga.txt"),'r');

        while(!feof($content)){

            $line = fgets($content);

            $new_city = $array = explode("|", $line);


            DB::table('cities')->upsert(
            [
                "d_codigo" => $new_city[0],
                "d_asenta" => $new_city[1],
                "d_tipo_asenta" => $new_city[2],
                "D_mnpio" => $new_city[3],
                "d_estado" => $new_city[4],
                "d_ciudad" => $new_city[5],
                "d_CP" => $new_city[6],
                "c_estado" => $new_city[7],
                "c_oficina" => $new_city[8],
                "c_CP" => $new_city[9],
                "c_tipo_asenta" => $new_city[10],
                "c_mnpio" => $new_city[11],
                "id_asenta_cpcons" => $new_city[12],
                "d_zona" => $new_city[13],
                "c_cve_ciudad" => $new_city[14]

            ],
            [
                "d_codigo" => $new_city[0],
            ]);
        }

        
    }
}
